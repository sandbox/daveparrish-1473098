; Modeled from the buildkit make file here:
; http://drupalcode.org/project/buildkit.git/blob_plain/122d308ca2d103484b54958ca35e2797fb0215c0:/drupal-org.make

api = 2
core = 7.x


; Modules =====================================================================
projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc1

projects[drupalforfirebug][subdir] = contrib
projects[drupalforfirebug][version] = 1.2

projects[devel][subdir] = contrib
projects[devel][version] = 1.2

; Add the modules you want to hack on here ====================================
projects[feeds][type] = module
projects[feeds][subdir] = hack
projects[feeds][download][type] = git
projects[feeds][downlaod][url] = http://git.drupal.org/project/feeds.git
projects[feeds][download][branch] = 7.x-2.x
