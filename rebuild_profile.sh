#!/bin/bash
echo "Please make sure you have all of your changes checked into git for modules and themes!"
echo "Press 'enter' to continue or 'ctrl-c' to quit."
read

# Get the backup folder
backupDir=.backups/rebuild-$(date +%F-%s)
mkdir -p $backupDir

echo "Deleting modules, themes and libraries directories."
mv modules $backupDir
mv themes $backupDir
mv libraries $backupDir

echo "Rebuilding profile from make file."
drush make $1 --working-copy --no-core --contrib-destination=. --no-gitinfofile devstart.make
git checkout modules
git checkout themes
git checkout libraries

# Update database just in case it is needed for an updated module
drush updb
