#!/bin/bash

# This script will reinstall the profile, meaning it will delete the site
# database and files directory and then reinstall the database file.
#
# This script assumes that the local user has permissions to remove files
# created by the webserver.  If it does not then it will not be able to remove
# the Drupal 'files' directory to reinstall the site.

# Get the backup folder
backupDir=.backups/$(date +%F-%s)

# removed the files directory if possible
filesDir=$(drush dd files)
if [ -n "$filesDir" ]; then
  # Create a backup folder (move because it is safer)
  mkdir -p $backupDir
  echo "mv $filesDir/* $backupDir"
  mv $filesDir/* $backupDir
fi

# Reinstall the site
chmod +w ../../sites/default
chmod +w ../../sites/default/settings.php
drush si devstart --account-mail=dev@example.com --site-name="Development Starter" --site-mail=dev@example.com --account-pass="admin"
chmod -w ../../sites/default
chmod -w ../../sites/default/settings.php

# Clear cache
drush cc all
