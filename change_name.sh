#!/bin/bash

# The purpose of this script is to change the name of the project to what you want.
# For example, change the name from 'devstart' to 'my_project' or 'upgradeya_dot_com'
# you could simply run './change_name.sh my_project' or './change_name.sh upgradeya_dot_com'

# Verify that the first parameter is present.
if [ -z "$1" ]
then
  echo "Parameter 1 missing: Must enter the project name name to change to."
  exit
fi

# Verify that the first parameter is alphanumeric
if ! grep -e '^\w*$' <<<$1 ;
then
  echo "Parameter 1 is not alphanumeric, please enter an alphanumeric name."
  exit
fi

# Get the current name which we will assume is the first part of the .make file
# TODO: quite if there are more than one matches on *.make
distro_files="*.make"
regex="^(\w*).make$"
for f in $distro_files
do
  [[ $f =~ $regex ]]
  distro_name="${BASH_REMATCH[1]}"
done

# Rename all instances of name in project files
sed -e "s/${distro_name}/$1/g" ${distro_name}.install > $1.install 
rm ${distro_name}.install
sed -e "s/${distro_name}/$1/g" ${distro_name}.profile > $1.profile
rm ${distro_name}.profile
sed -i -e "s/${distro_name}/$1/g" reinstall_profile.sh
sed -i -e "s/${distro_name}/$1/g" rebuild_profile.sh

# Rename .make and .info
mv ${distro_name}.make $1.make
mv ${distro_name}.info $1.info
