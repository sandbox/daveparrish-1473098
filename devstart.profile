<?php

/**
 * Implements hook_form_install_FORM_alter()
 *
 * Change the form to try and fill in some data that we probably already know.
 **/
function devstart_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate as much site information as we can
  $form['site_information']['site_name']['#default_value'] = "Development Start";
  $form['site_information']['site_mail']['#default_value'] = "dev@example.com";
  $form['admin_account']['account']['name']['#default_value'] = "admin";
  $form['admin_account']['account']['mail']['#default_value'] = "dev@example.com";
  $form['server_settings']['site_default_country']['#default_value'] = "US";
  $form['server_settings']['date_default_timezone']['#default_value'] = "America/New_York";
  $form['update_notifications']['update_status_module']['#default_value'] = array(1);
}

/**
 * Implements hook_install_tasks()
 *
 * Source: http://drupal.org/node/1151914
 */
//function devstart_install_tasks($install_state) {
//  // Create tasks for importing data
//  $task['default_content'] = array(
//    'display_name' => st('Add default content'),
//    'display' => TRUE,
//    'type' => 'batch',
//    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED, // default to insert content
//    'function' => 'devstart_import_default_feeds',
//  );
//
//  // Various configurations to set
//  $task['default_configurations'] = array(
//    'display_name' => st('Set default configurations'),
//    'display' => TRUE,
//    'type' => 'normal',
//    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED, // default to insert content
//    'function' => 'devstart_default_configurations',
//  );
//
//  return $task;
//}

/**
 * Callback for devstart_install_tasks()
 *
 * Installs default configurations for Development Starter
 *
 * Here we configure everything missed by features which we want to have setup
 * by default
 */
//function devstart_default_configurations($install_state) {
//  return(null);
//}

/**
 * Callback for devstart_install_tasks()
 *
 * Installs default content using the Feeds API
 *
 * Source: http://drupal.org/node/1151914
 */
//function devstart_import_default_feeds($install_state) {
//  $root = drupal_get_path('profile', 'devstart') . '/data/';
//  $files = array(
//    'user_importer' => 'user_data.csv',
//    'basic_page_importer' => 'basic_page_data.csv',
//  );
//
//  $operations = array();
//
//  foreach ($files as $importer => $file) {
//    // Get the importer
//    $importerSource = feeds_source($importer);
//
//    $filename = $root . $file;
//    if (!file_destination($filename, FILE_EXISTS_ERROR)) {
//
//      // Set the importers source
//      $config = $importerSource->getConfig();
//      $config['FeedsFileFetcher']['source'] = $filename;
//      $importerSource->setConfig($config);
//      $importerSource->save();
//
//      $operations[] = array(
//        'feeds_batch',
//        array('import',$importer,0),
//      );
//    } // TODO: Print an error here if file was not found
//  }
//
//  $batch = array(
//    'title' => t('Importing feeds'),
//    'operations' => $operations,
//    'progress_message' => t('Current: @current | Remaining:
//      @remaining | Total: @total | Percentage: @percentage | Estimate:
//      @estimate | Elapsed @elapsed'),
//  );
//
//  return($batch);
//}
